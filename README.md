# arpav-meteo #
**Contributors:**      Marco Bonomo
**Donate link:**       http://www.alpedelnevegal.it
**Tags:**
**Requires at least:** 3.6.0
**Tested up to:**      3.6.0
**Stable tag:**        0.1.0
**License:**           GPLv2
**License URI:**       http://www.gnu.org/licenses/gpl-2.0.html

## Description ##

a wordpress plugin for display meteo info

## Installation ##

### Manual Installation ###

1. Upload the entire `/arpav-meteo` directory to the `/wp-content/plugins/` directory.
2. Activate arpav-meteo through the 'Plugins' menu in WordPress.

## Frequently Asked Questions ##


## Screenshots ##


## Changelog ##

### 0.1.0 ###
* First release
